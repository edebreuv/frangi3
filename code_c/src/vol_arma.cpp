// Copyright CNRS/Inria/UNS
// Contributor(s): Eric Debreuve (since 2019)
//
// eric.debreuve@cnrs.fr
//
// This software is governed by the CeCILL  license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
//"http://www.cecill.info".
//
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.
//
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.
//
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#include "vol_arma.h"

#include <iostream>
#include <stdio.h>

typedef arma::Col<float> kernel1_t;

void FromArmadilloToRaw(const volume_t& volume, float* array)
{
    arma::Cube<float>::const_iterator elm_pointer = volume.begin();
    arma::Cube<float>::const_iterator end_position = volume.end();

    for (; elm_pointer != end_position; elm_pointer++, array++) {
        *array = *elm_pointer;
    }
}

void SmoothVolumeWithGaussian(const volume_t& volume, volume_t& smoothed, float scale)
{
    int kernel_width = (int)(6.0f * scale);
    if (kernel_width % 2 == 0) {
        kernel_width++;
    }
    int k_half_width = kernel_width / 2;
    kernel1_t gaussian = arma::normpdf(arma::linspace<kernel1_t>(-k_half_width,
                                           k_half_width, kernel_width),
        0.0f, scale);
    float sum_coeff = 0.0f;
    for (int idx = 0; idx < kernel_width; idx++) {
        sum_coeff += gaussian.at(idx); // Use at<float>(idx) w/ OpenCV
    }

    //    int kernel_width = 2 * ((int)(roundf(2.0f * scale) / 2.0f)) + 1;
    //    int k_half_width = kernel_width / 2;
    //    kernel1_t gaussian(kernel_width, arma::fill::ones);
    //    float sum_coeff = (float) kernel_width;

    long long n_rows = (long long)volume.n_rows;
    long long n_cols = (long long)volume.n_cols;
    long long n_slices = (long long)volume.n_slices;

    if (n_cols > (long long)k_half_width) {
        for (long long dep = 0; dep < n_slices; dep++)
            for (long long col = 0; col < (long long)k_half_width; col++)
                for (long long row = 0; row < n_rows; row++) {
                    long long shifted_col = col - k_half_width;
                    float sum_vol = 0.0f, sum_local_coeff = 0.0f;
                    for (int idx = k_half_width - col; idx < kernel_width; idx++) {
                        float g_coeff = gaussian.at(idx);
                        sum_vol += g_coeff * volume.at(row, shifted_col + idx, dep);
                        sum_local_coeff += g_coeff;
                    }
                    smoothed.at(row, col, dep) = sum_vol / sum_local_coeff;
                }
        for (long long dep = 0; dep < n_slices; dep++)
            for (long long col = n_cols - k_half_width; col < n_cols; col++)
                for (long long row = 0; row < n_rows; row++) {
                    long long shifted_col = col - k_half_width;
                    float sum_vol = 0.0f, sum_local_coeff = 0.0f;
                    for (int idx = 0; idx < (int)(n_cols + k_half_width - col); idx++) {
                        float g_coeff = gaussian.at(idx);
                        sum_vol += g_coeff * volume.at(row, shifted_col + idx, dep);
                        sum_local_coeff += g_coeff;
                    }
                    smoothed.at(row, col, dep) = sum_vol / sum_local_coeff;
                }
#pragma omp parallel for
        for (long long dep = 0; dep < n_slices; dep++)
            for (long long col = k_half_width; col < n_cols - k_half_width; col++)
                for (long long row = 0; row < n_rows; row++) {
                    long long shifted_col = col - k_half_width;
                    float sum_vol = 0.0f;
                    for (int idx = 0; idx < kernel_width; idx++) {
                        sum_vol += gaussian.at(idx) * volume.at(row, shifted_col + idx, dep);
                    }
                    smoothed.at(row, col, dep) = sum_vol / sum_coeff;
                }
    } else {
        smoothed = volume;
    }

    volume_t smoothed_cr(arma::size(volume));
    if (n_rows > (long long)k_half_width) {
        for (long long dep = 0; dep < n_slices; dep++)
            for (long long col = 0; col < n_cols; col++)
                for (long long row = 0; row < (long long)k_half_width; row++) {
                    long long shifted_row = row - k_half_width;
                    float sum_vol = 0.0f, sum_local_coeff = 0.0f;
                    for (int idx = k_half_width - row; idx < kernel_width; idx++) {
                        float g_coeff = gaussian.at(idx);
                        sum_vol += g_coeff * smoothed.at(shifted_row + idx, col, dep);
                        sum_local_coeff += g_coeff;
                    }
                    smoothed_cr.at(row, col, dep) = sum_vol / sum_local_coeff;
                }
        for (long long dep = 0; dep < n_slices; dep++)
            for (long long col = 0; col < n_cols; col++)
                for (long long row = n_rows - k_half_width; row < n_rows; row++) {
                    long long shifted_row = row - k_half_width;
                    float sum_vol = 0.0f, sum_local_coeff = 0.0f;
                    for (int idx = 0; idx < (int)(n_rows + k_half_width - row); idx++) {
                        float g_coeff = gaussian.at(idx);
                        sum_vol += g_coeff * smoothed.at(shifted_row + idx, col, dep);
                        sum_local_coeff += g_coeff;
                    }
                    smoothed_cr.at(row, col, dep) = sum_vol / sum_local_coeff;
                }
#pragma omp parallel for
        for (long long dep = 0; dep < n_slices; dep++)
            for (long long col = 0; col < n_cols; col++)
                for (long long row = k_half_width; row < n_rows - k_half_width; row++) {
                    long long shifted_row = row - k_half_width;
                    float sum_vol = 0.0f;
                    for (int idx = 0; idx < kernel_width; idx++) {
                        sum_vol += gaussian.at(idx) * smoothed.at(shifted_row + idx, col, dep);
                    }
                    smoothed_cr.at(row, col, dep) = sum_vol / sum_coeff;
                }
    } else {
        smoothed_cr = smoothed;
    }

    if (n_slices > (long long)k_half_width) {
        for (long long dep = 0; dep < (long long)k_half_width; dep++)
            for (long long col = 0; col < n_cols; col++)
                for (long long row = 0; row < n_rows; row++) {
                    long long shifted_dep = dep - k_half_width;
                    float sum_vol = 0.0f, sum_local_coeff = 0.0f;
                    for (int idx = k_half_width - dep; idx < kernel_width; idx++) {
                        float g_coeff = gaussian.at(idx);
                        sum_vol += g_coeff * smoothed_cr.at(row, col, shifted_dep + idx);
                        sum_local_coeff += g_coeff;
                    }
                    smoothed.at(row, col, dep) = sum_vol / sum_local_coeff;
                }
        for (long long dep = n_slices - k_half_width; dep < n_slices; dep++)
            for (long long col = 0; col < n_cols; col++)
                for (long long row = 0; row < n_rows; row++) {
                    long long shifted_dep = dep - k_half_width;
                    float sum_vol = 0.0f, sum_local_coeff = 0.0f;
                    for (int idx = 0; idx < (int)(n_slices + k_half_width - dep); idx++) {
                        float g_coeff = gaussian.at(idx);
                        sum_vol += g_coeff * smoothed_cr.at(row, col, shifted_dep + idx);
                        sum_local_coeff += g_coeff;
                    }
                    smoothed.at(row, col, dep) = sum_vol / sum_local_coeff;
                }
#pragma omp parallel for
        for (long long dep = k_half_width; dep < n_slices - k_half_width; dep++)
            for (long long col = 0; col < n_cols; col++)
                for (long long row = 0; row < n_rows; row++) {
                    long long shifted_dep = dep - k_half_width;
                    float sum_vol = 0.0f;
                    for (int idx = 0; idx < kernel_width; idx++) {
                        sum_vol += gaussian.at(idx) * smoothed_cr.at(row, col, shifted_dep + idx);
                    }
                    smoothed.at(row, col, dep) = sum_vol / sum_coeff;
                }
    } else {
        smoothed = smoothed_cr;
    }
}

// void
// PrintVolume(const volume_t &volume)
//{
//     std::cout << "Volume[" << volume.n_rows << "," << volume.n_cols << "," <<
//               volume.n_slices << "]" << std::endl;

//    for (unsigned long long dep = 0; dep < volume.n_slices; dep++)
//        for (unsigned long long col = 0; col < volume.n_cols; col++)
//            for (unsigned long long row = 0; row < volume.n_rows; row++) {
//                std::cout << "(" << row << "," << col << "," << dep << ")=" << volume.at(row, col,
//                          dep) << std::endl;
//            }

//    std::cout << "---" << std::endl;
//}
