// Copyright CNRS/Inria/UNS
// Contributor(s): Eric Debreuve (since 2019)
//
// eric.debreuve@cnrs.fr
//
// This software is governed by the CeCILL  license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
//"http://www.cecill.info".
//
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.
//
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.
//
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#include "frangi3.h"

#include "hessian.h"
#include "vol_arma.h"

#include <armadillo>
#include <iostream>
#include <math.h>
#include <stdio.h>

const int _margin_g = 1;
const float _one_Gb_as_float = 1024.0 * 1024.0 * 1024.0;

static void
_ComputeResponse(
    const volume_t& volume,
    bool bright_on_dark,
    volume_t& frangi, volume_t direction[3], volume_t& scale,
    float min_scale, float max_scale, float scale_step,
    float plate_factor, float blob_factor, float bckgnd_factor);

static void
_ComputeLocalResponse(const volume_t& volume,
    bool bright_on_dark,
    volume_t& frangi, volume_t direction[3],
    int row, int col, int dep,
    float plate_factor, float blob_factor, float bckgnd_factor);

extern "C" {
void ComputeFrangiResponseFromRaw(
    const float* raw_volume,
    int width, int height, int depth,
    bool bright_on_dark,
    float* raw_frangi, float* raw_scale,
    float min_scale, float max_scale, float scale_step,
    float alpha, float beta, float gamma)
{
    int n_rows = height;
    int n_cols = width;
    int n_slices = depth;

    float plate_factor = -0.5f / (alpha * alpha);
    float blob_factor = -0.5f / (beta * beta);
    float bckgnd_factor = -0.5f / (gamma * gamma);

    printf("    Allocating 6 %dx%dx%d-volumes = %.1fGb\n",
        n_rows, n_cols, n_slices,
        (6.0 * n_rows * n_cols * n_slices * sizeof(float)) / _one_Gb_as_float);
    volume_t volume(raw_volume, n_rows, n_cols, n_slices);
    volume_t frangi(arma::size(volume));
    volume_t direction[3];
    volume_t scale(arma::size(volume));
    for (int idx = 0; idx < 3; idx++) {
        direction[idx].set_size(arma::size(volume));
    }

    _ComputeResponse(volume, bright_on_dark, frangi, direction, scale,
        min_scale, max_scale, scale_step,
        plate_factor, blob_factor, bckgnd_factor);

    //     Data3D<FrangiResponse_Sig> vn_sig_nms;
    //     IP::non_max_suppress( vn_sig, vn_sig_nms );
    //
    //     Data3D<FrangiResponse_Sig> vn_sig_et;
    //     IP::edge_tracing( vn_sig_nms, vn_sig_et, 0.45f, 0.05f );

    FromArmadilloToRaw(frangi, raw_frangi);
    FromArmadilloToRaw(scale, raw_scale);

    printf("    De-allocating 6 volumes\n");
    std::cout << "    Frangi3: Done" << std::endl
              << std::flush;
}
}

static void
_ComputeResponse(
    const volume_t& volume,
    bool bright_on_dark,
    volume_t& frangi, volume_t direction[3], volume_t& scale,
    float min_scale, float max_scale, float scale_step,
    float plate_factor, float blob_factor, float bckgnd_factor)
{
    printf("    Allocating 5 %lldx%lldx%lld-volumes = %.1fGb\n",
        volume.n_rows, volume.n_cols, volume.n_slices,
        (5.0 * volume.n_rows * volume.n_cols * volume.n_slices * sizeof(float)) / _one_Gb_as_float);

    volume_t smoothed(arma::size(volume));
    volume_t frg_at_scale(arma::size(volume));
    volume_t dir_at_scale[3];
    for (int idx = 0; idx < 3; idx++) {
        dir_at_scale[idx].set_size(arma::size(volume));
    }

    // Filling frangi with -1 ensures that condition (*) below is true when
    // scl == min_scale so that frangi, direction and scale are "fully" initialized,
    // except for the _margin_g.
    frangi.fill(-1.0f);
    frangi.row(0).fill(0.0f);
    frangi.row(frangi.n_rows - 1).fill(0.0f);
    frangi.col(0).fill(0.0f);
    frangi.col(frangi.n_cols - 1).fill(0.0f);
    frangi.slice(0).fill(0.0f);
    frangi.slice(frangi.n_slices - 1).fill(0.0f);

    scale.row(0).fill(min_scale);
    scale.row(scale.n_rows - 1).fill(min_scale);
    scale.col(0).fill(min_scale);
    scale.col(scale.n_cols - 1).fill(min_scale);
    scale.slice(0).fill(min_scale);
    scale.slice(scale.n_slices - 1).fill(min_scale);

    volume_t* dir_cursor = direction;
    for (int idx = 0; idx < 3; idx++) {
        dir_cursor->row(0).fill(0.0f);
        dir_cursor->row(scale.n_rows - 1).fill(0.0f);
        dir_cursor->col(0).fill(0.0f);
        dir_cursor->col(scale.n_cols - 1).fill(0.0f);
        dir_cursor->slice(0).fill(0.0f);
        dir_cursor->slice(scale.n_slices - 1).fill(0.0f);
        dir_cursor++;
    }

    for (float scl = min_scale; scl <= max_scale; scl += scale_step) {
        std::cout << "    Frangi@scale " << scl << std::endl
                  << std::flush;

        if (scl > 0.0f) {
            SmoothVolumeWithGaussian(volume, smoothed, scl);
        } else {
            smoothed = volume;
        }
        smoothed *= scl * scl;

#pragma omp parallel for
        for (unsigned long long dep = _margin_g; dep < volume.n_slices - _margin_g; dep++)
            for (unsigned long long col = _margin_g; col < volume.n_cols - _margin_g; col++)
                for (unsigned long long row = _margin_g; row < volume.n_rows - _margin_g; row++) {
                    _ComputeLocalResponse(smoothed, bright_on_dark,
                        frg_at_scale, dir_at_scale,
                        row, col, dep,
                        plate_factor, blob_factor, bckgnd_factor);

                    if (frangi.at(row, col, dep) < frg_at_scale.at(row, col, dep)) { // Condition (*)
                        frangi.at(row, col, dep) = frg_at_scale.at(row, col, dep);
                        scale.at(row, col, dep) = scl;
                        for (int idx = 0; idx < 3; idx++) {
                            direction[idx].at(row, col, dep) = dir_at_scale[idx].at(row, col, dep);
                        }
                    }
                }
    }

    printf("    De-allocating 5 volumes\n");
}

static void
_ComputeLocalResponse(const volume_t& volume_at_scale,
    bool bright_on_dark,
    volume_t& frangi, volume_t direction[3],
    int row, int col, int dep,
    float plate_factor, float blob_factor, float bckgnd_factor)
{
    arma::Mat<float>::fixed<3, 3> hessian_matrix;
    ComputeLocalHessianMatrix(volume_at_scale, row, col, dep, hessian_matrix);

    arma::Col<float>::fixed<3> eigenvalues;
    arma::Mat<float>::fixed<3, 3> eigenvectors;
    arma::eig_sym(eigenvalues, eigenvectors, hessian_matrix);

    // order eigenvalues so that |lambda1| < |lambda2| < |lambda3|
    int first = 0, second = 1, third = 2;
    float abs_eigenvalues[3];
    for (int idx = 0; idx < 3; idx++) {
        abs_eigenvalues[idx] = fabsf(eigenvalues.at(idx));
    }
    if (abs_eigenvalues[first] > abs_eigenvalues[second]) {
        std::swap(first, second);
    }
    if (abs_eigenvalues[second] > abs_eigenvalues[third]) {
        std::swap(second, third);
    }
    if (abs_eigenvalues[first] > abs_eigenvalues[second]) {
        std::swap(first, second);
    }

    if (bright_on_dark && ((eigenvalues.at(second) > 0.0f) || (eigenvalues.at(third) > 0.0f))) {
        frangi.at(row, col, dep) = 0.0f;
    } else if ((!bright_on_dark) && ((eigenvalues.at(second) < 0.0f) || (eigenvalues.at(third) < 0.0f))) {
        frangi.at(row, col, dep) = 0.0f;
    } else if (abs_eigenvalues[second] == 0.0f) {
        frangi.at(row, col, dep) = 0.0f;
    } else {
        float plate = abs_eigenvalues[second] / abs_eigenvalues[third];
        float blob = abs_eigenvalues[first] / sqrtf(abs_eigenvalues[second] * abs_eigenvalues[third]);
        float bckgnd = sqrtf(abs_eigenvalues[first] * abs_eigenvalues[first] + abs_eigenvalues[second] * abs_eigenvalues[second] + abs_eigenvalues[third] * abs_eigenvalues[third]);
        frangi.at(row, col, dep) = (1.0f - expf(plate_factor * plate * plate))
            * expf(blob_factor * blob * blob)
            * (1.0f - expf(bckgnd_factor * bckgnd * bckgnd));
    }

    // direction: eigenvector of the smallest eigenvalue
    for (int idx = 0; idx < 3; idx++) {
        direction[idx].at(row, col, dep) = eigenvectors.at(idx, first);
    }
}
