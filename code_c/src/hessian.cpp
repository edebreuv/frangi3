// Copyright CNRS/Inria/UNS
// Contributor(s): Eric Debreuve (since 2019)
//
// eric.debreuve@cnrs.fr
//
// This software is governed by the CeCILL  license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
//"http://www.cecill.info".
//
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.
//
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.
//
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#include "hessian.h"

#include "vol_arma.h"
#include <iostream>

// const int _margin_g = 1;

void ComputeLocalHessianMatrix(const volume_t& volume,
    int row, int col, int dep,
    arma::Mat<float>::fixed<3, 3>& hessian_matrix)
{
    float twice = 2.0f * volume.at(row, col, dep);
    float d_row_2 = volume.at(row - 1, col, dep) - twice + volume.at(row + 1, col, dep);
    float d_col_2 = volume.at(row, col - 1, dep) - twice + volume.at(row, col + 1, dep);
    float d_dep_2 = volume.at(row, col, dep - 1) - twice + volume.at(row, col, dep + 1);

    float d_row_col = (volume.at(row - 1, col - 1, dep)
                          + volume.at(row + 1, col + 1, dep)
                          - volume.at(row + 1, col - 1, dep)
                          - volume.at(row - 1, col + 1, dep))
        * 0.25f;
    float d_row_dep = (volume.at(row - 1, col, dep - 1)
                          + volume.at(row + 1, col, dep + 1)
                          - volume.at(row + 1, col, dep - 1)
                          - volume.at(row - 1, col, dep + 1))
        * 0.25f;
    float d_col_dep = (volume.at(row, col - 1, dep - 1)
                          + volume.at(row, col + 1, dep + 1)
                          - volume.at(row, col + 1, dep - 1)
                          - volume.at(row, col - 1, dep + 1))
        * 0.25f;

    // hessian_matrix.set_size(3, 3);

    hessian_matrix.at(0, 0) = d_row_2;
    hessian_matrix.at(0, 1) = d_row_col;
    hessian_matrix.at(0, 2) = d_row_dep;

    hessian_matrix.at(1, 0) = d_row_col;
    hessian_matrix.at(1, 1) = d_col_2;
    hessian_matrix.at(1, 2) = d_col_dep;

    hessian_matrix.at(2, 0) = d_row_dep;
    hessian_matrix.at(2, 1) = d_col_dep;
    hessian_matrix.at(2, 2) = d_dep_2;
}

// void
// ComputeHessianMatrixFeatures(const volume_t &volume, int features,
//                              volume_t &out_hessians)
//{
//     out_hessians.set_size(arma::size(volume));

//    out_hessians.row(0).fill(0.0f);
//    out_hessians.col(0).fill(0.0f);
//    out_hessians.slice(0).fill(0.0f);
//    out_hessians.row(out_hessians.n_rows - 1).fill(0.0f);
//    out_hessians.col(out_hessians.n_cols - 1).fill(0.0f);
//    out_hessians.slice(out_hessians.n_slices - 1).fill(0.0f);

//    //#pragma omp parallel for
//    for (unsigned long long dep = _margin_g; dep < out_hessians.n_slices - _margin_g; dep++)
//        for (unsigned long long col = _margin_g; col < out_hessians.n_cols - _margin_g; col++)
//            for (unsigned long long row = _margin_g; row < out_hessians.n_rows - _margin_g; row++) {
//                arma::Mat<float> hessian_matrix;
//                ComputeLocalHessianMatrix(volume, row, col, dep, hessian_matrix);

//                if ((features == 0) || (features == 1) || (features == 2)) {
//                    arma::Col<float> eigenvalues;
//                    arma::eig_sym(eigenvalues, hessian_matrix);
//                    out_hessians.at(row, col, dep) = eigenvalues.at(features);
//                }
//                else if ((features == -1) || (features == -2) || (features == -3)) {
//                    arma::Col<float> eigenvalues;
//                    arma::eig_sym(eigenvalues, hessian_matrix);
//                    arma::Col<float> abs_eigenvalues = arma::sort(arma::abs(eigenvalues));
//                    out_hessians.at(row, col, dep) = abs_eigenvalues.at(-1 - features);
//                }
//                else if (features == 4) {
//                    arma::Mat<float> hessian_abs = arma::abs(hessian_matrix);
//                    out_hessians.at(row, col, dep) = hessian_abs.min();
//                }
//                else if (features == 5) {
//                    arma::Mat<float> hessian_abs = arma::abs(hessian_matrix);
//                    out_hessians.at(row, col, dep) = hessian_abs.max();
//                }
//                else if (features == 6) {
//                    arma::Mat<float> hessian_abs = arma::abs(hessian_matrix);
//                    out_hessians.at(row, col, dep) = 0.5 * (hessian_abs.min() + hessian_abs.max());
//                }
//            }
//}
