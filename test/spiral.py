# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2019)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import itertools as ittl
import time
from typing import Sequence

import matplotlib.pyplot as pypl
import numpy as nmpy
import scipy.ndimage as spim
import skimage.draw as draw
import skimage.measure as msre
import skimage.morphology as mrph
from mpl_toolkits.mplot3d import Axes3D

from frangi3 import FrangiEnhancement


array_t = nmpy.ndarray


# Free parameters
PARALLEL_CHOICES = (False, True)
METHOD_CHOICES = ("python", "c")
DIFFERENTIATION_CHOICES = ("FD11", "FD2", "convolution")

SHAPE = (200, 200, 200)
MARGIN = 4

SCALE_RANGE = (0.1, 6.1)
SCALE_STEP = 1.0

# Spiral parameters
N_SAMPLES = 500
ANGLE_MAX = 50.0
HEIGHT_MAX = 2.0
ROTATION_SPEED = 0.05
RADIAL_SPEED = 0.05


def Spiral(
    n_samples: int,
    angle_max: float,
    height_max: float,
    rotation_speed: float,
    radial_speed: float,
    /,
) -> tuple[array_t, array_t, array_t]:
    """"""
    angles = nmpy.linspace(0.0, angle_max, n_samples)
    x_coords = rotation_speed * nmpy.exp(radial_speed * angles) * nmpy.cos(angles)
    y_coords = rotation_speed * nmpy.exp(radial_speed * angles) * nmpy.sin(angles)
    z_coords = nmpy.linspace(0.0, height_max, n_samples)

    return x_coords, y_coords, z_coords


def ScaledAndShiftedAndPossiblyRounded(
    x_coords: array_t,
    y_coords: array_t,
    z_coords: array_t,
    target_bbox: Sequence[int],
    /,
    *,
    margin: int = 0,
    rounded: bool = False,
) -> tuple[array_t, array_t, array_t]:
    """"""
    if target_bbox.__len__() == 3:  # It is a shape
        target_bbox = (
            0,
            target_bbox[0] - 1,
            0,
            target_bbox[1] - 1,
            0,
            target_bbox[2] - 1,
        )

    corner = nmpy.array((nmpy.min(x_coords), nmpy.min(y_coords), nmpy.min(z_coords)))
    shape = nmpy.diff(
        (
            corner[0],
            nmpy.max(x_coords),
            corner[1],
            nmpy.max(y_coords),
            corner[2],
            nmpy.max(z_coords),
        )
    )[0::2]
    # Even if dealing with indices, do not do + 1
    target_shape = nmpy.diff(target_bbox)[0::2] - 2 * margin

    scaling = target_shape / shape
    shift = tuple(tgt_coord + margin for tgt_coord in target_bbox[0::2])

    x_coords = scaling[0] * (x_coords - corner[0]) + shift[0]
    y_coords = scaling[1] * (y_coords - corner[1]) + shift[1]
    z_coords = scaling[2] * (z_coords - corner[2]) + shift[2]

    if rounded:
        x_coords = nmpy.around(x_coords)
        y_coords = nmpy.around(y_coords)
        z_coords = nmpy.around(z_coords)

    return x_coords, y_coords, z_coords


def PlotCurveInArray(
    x_coords: array_t,
    y_coords: array_t,
    z_coords: array_t,
    volume: array_t,
    /,
    *,
    target_bbox: Sequence[int] = None,
    thickness: int = 1,
) -> None:
    """"""
    coords = []  # curr_x, next_x, curr_y, next_y...
    for coord in (x_coords, y_coords, z_coords):
        iterators = ittl.tee(coord)
        next(iterators[1])  # Should it be next(iterators[1], None)?
        coords.extend(iterators)

    for curr_x, next_x, curr_y, next_y, curr_z, next_z in zip(*coords):
        line = draw.line_nd(
            (curr_x, curr_y, curr_z),
            (next_x, next_y, next_z),
            endpoint=True,
        )

        volume[line] = 1

    if thickness > 1:
        selem = mrph.ball(thickness // 2, dtype=nmpy.bool_)
        if target_bbox is None:
            dilated = spim.binary_dilation(volume, structure=selem).astype(
                nmpy.uint8, copy=False
            )
            volume[...] = dilated[...]
        else:
            x_slice = slice(target_bbox[0], target_bbox[1] + 1)
            y_slice = slice(target_bbox[2], target_bbox[3] + 1)
            z_slice = slice(target_bbox[4], target_bbox[5] + 1)
            dilated = spim.binary_dilation(
                volume[x_slice, y_slice, z_slice], structure=selem
            ).astype(nmpy.uint8, copy=False)
            volume[x_slice, y_slice, z_slice] = dilated[...]


def PrepareCurvePlot(
    x_coords: array_t, y_coords: array_t, z_coords: array_t, /, *, title: str = None
) -> None:
    """"""
    fig = pypl.figure()
    ax = fig.add_subplot(111, projection=Axes3D.name)
    ax.plot(x_coords, y_coords, z_coords)
    ax.set_xlabel("x")
    ax.set_ylabel("y")
    ax.set_zlabel("z")
    if title is not None:
        ax.set_title(title)


def PrepareIsoSurfacePlot(volume: array_t, /, *, title: str = None) -> None:
    """"""
    fig = pypl.figure()
    ax = fig.add_subplot(111, projection=Axes3D.name)
    verts, faces, *_ = msre.marching_cubes(
        volume, level=0.0, spacing=(0.5, 0.5, 0.5), method="lorensen"
    )
    ax.plot_trisurf(verts[:, 0], verts[:, 1], faces, verts[:, 2], cmap="Spectral", lw=1)
    ax.set_xlabel("x")
    ax.set_ylabel("y")
    ax.set_zlabel("z")
    if title is not None:
        ax.set_title(title)


def Main() -> None:
    """"""
    print(f"STARTED: {time.strftime('%a, %b %d %Y @ %H:%M:%S')}")
    start_time = time.time()

    print("--- Volume Creation")

    volume = nmpy.zeros(SHAPE, dtype=nmpy.uint8)
    x_local, y_local, z_local = Spiral(
        N_SAMPLES, ANGLE_MAX, HEIGHT_MAX, ROTATION_SPEED, RADIAL_SPEED
    )

    half_shape = tuple(dim // 2 for dim in SHAPE)
    thickness = 1
    for x_range in ((0, half_shape[0]), (half_shape[0] + 1, SHAPE[0] - 1)):
        for y_range in ((0, half_shape[1]), (half_shape[1] + 1, SHAPE[1] - 1)):
            for z_range in ((0, half_shape[2]), (half_shape[2] + 1, SHAPE[2] - 1)):
                print(f"    Thickness: {thickness}")
                target_bbox = x_range + y_range + z_range
                x_global, y_global, z_global = ScaledAndShiftedAndPossiblyRounded(
                    x_local,
                    y_local,
                    z_local,
                    target_bbox,
                    margin=MARGIN,
                    rounded=True,
                )
                PlotCurveInArray(
                    x_global,
                    y_global,
                    z_global,
                    volume,
                    target_bbox=target_bbox,
                    thickness=thickness,
                )
                thickness += 1

    print("--- Display Preparation")

    PrepareIsoSurfacePlot(volume, title="Original Volume")

    print("--- Done")

    useless_choices = (
        (False, "c", "indirect"),
        (True, "c", "indirect"),
        (True, "c", "direct"),
    )
    prm_choices = []
    for parallel_prm in PARALLEL_CHOICES:
        for method_prm in METHOD_CHOICES:
            for differentiation_prm in DIFFERENTIATION_CHOICES:
                choice = (parallel_prm, method_prm, differentiation_prm)
                if choice not in useless_choices:
                    prm_choices.append(choice)

    for idx, parameters in enumerate(prm_choices, start=1):
        prm_as_str = "/".join(elm.__str__().capitalize() for elm in parameters)
        prm_as_str = prm_as_str.replace("False", "Sequential")
        prm_as_str = prm_as_str.replace("True", "Parallel")

        elapsed_time = time.gmtime(time.time() - start_time)
        print(f"\nElapsed Time={time.strftime('%Hh %Mm %Ss', elapsed_time)}")

        print(f"--- Frangi Enhancement {idx} of {prm_choices.__len__()}: {prm_as_str}")

        enhanced, scale_map = FrangiEnhancement(
            volume,
            SCALE_RANGE,
            scale_step=SCALE_STEP,
            in_parallel=parameters[0],
            method=parameters[1],
            differentiation_mode=parameters[2],
        )
        elapsed_time = time.gmtime(time.time() - start_time)
        print(f"\nElapsed Time={time.strftime('%Hh %Mm %Ss', elapsed_time)}")

        print("--- Display Preparation")

        PrepareIsoSurfacePlot(enhanced, title=f"Enhanced Volume {prm_as_str}")
        figure = pypl.figure()
        axes = figure.add_subplot(111)
        axes.matshow(nmpy.amax(scale_map, axis=2))

        print("--- Done")
        elapsed_time = time.gmtime(time.time() - start_time)
        print(f"\nElapsed Time={time.strftime('%Hh %Mm %Ss', elapsed_time)}")

    elapsed_time = time.gmtime(time.time() - start_time)
    print(f"\nElapsed Time={time.strftime('%Hh %Mm %Ss', elapsed_time)}")
    print(f"DONE: {time.strftime('%a, %b %d %Y @ %H:%M:%S')}")

    pypl.show()


if __name__ == "__main__":
    #
    Main()
