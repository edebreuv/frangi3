# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2019)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import time

import matplotlib.pyplot as pypl
import numpy as nmpy
import scipy.ndimage as spim
import skimage.filters as fltr
import skimage.morphology as mrph
from com_github_ellisdg_frangi3d.frangi import frangi as FRANGI

from frangi3 import FrangiEnhancement, ScalesFromRangeAndStep


# --- Bar parameters
RADII = tuple(range(1, 6, 2))
INTENSITIES = tuple(range(10, 101, 60))
BAR_N_ROWS_HINT = 50
BAR_ROW_SPACING = 10

# --- Enhancement parameters
SCALE_RANGE = (1.0, 5.0)
SCALE_STEP = 1.0
ALPHA = 0.5
BETA = 0.5 + 1.0e-9
FRANGI_C = 1.0

# --- Execution parameters
ALL_METHODS = (
    "ellisdg",
    "hessian",
    "meijering",
    "sato",
    "skimage",
)
METHODS = ALL_METHODS
PLOT_VOLUME = False


# --- Skeletonization
def SkeletonOfResponse(response: nmpy.ndarray, /) -> nmpy.ndarray:
    """"""
    normalized_response = response.copy()
    # normalized_response[volume > 0.0] /= volume[volume > 0.0]

    thresholded = normalized_response > 0.2 * nmpy.amax(normalized_response)

    output = mrph.skeletonize_3d(thresholded).astype(nmpy.uint8)
    output[output > 0] = 1

    return output


# --- Bar volume preparation
bar_n_rows = 2 * 3 * max(SCALE_RANGE) + BAR_N_ROWS_HINT
margin = 3 * max(SCALE_RANGE) + max(RADII)
row_spacing = BAR_ROW_SPACING
col_spacing = margin
shape = (
    2 * margin + INTENSITIES.__len__() * (bar_n_rows + row_spacing) - row_spacing,
    2 * margin + RADII.__len__() * (2 * margin + col_spacing) - col_spacing,
    2 * margin + 2 * max(RADII),
)

bar_n_rows = int(round(bar_n_rows))
margin = int(round(margin))
row_spacing = int(round(row_spacing))
col_spacing = int(round(col_spacing))
shape = tuple(int(round(length)) for length in shape)

print(f"Image lengths={shape}\nRadii={RADII}\n")

# --- Bar volume creation
volume_ = nmpy.zeros(shape, dtype=nmpy.uint16)
row = margin
for intensity in INTENSITIES:
    col = margin
    for radius in RADII:
        per_bar_volume = nmpy.zeros(shape, dtype=nmpy.bool_)
        per_bar_volume[
            row : (row + bar_n_rows), col + margin, max(RADII) + margin
        ] = True

        st_elm = mrph.disk(radius, dtype=nmpy.bool_)
        length = st_elm.shape[0]
        st_elm = nmpy.reshape(st_elm, (1, length, length))
        per_bar_volume = spim.binary_dilation(per_bar_volume, structure=st_elm)
        volume_[per_bar_volume] = intensity

        col += 2 * margin + col_spacing

    row += bar_n_rows + row_spacing
# This prevents skimage from rescaling the volume, which changes Frangi's output (it should be the responsibility of the
# user to rescale the volume if needed, and to choose the detection parameters in the appropriate ranges).
volume_ = volume_.astype(nmpy.float32)

# --- Bar volume display
if PLOT_VOLUME:
    ops = (nmpy.transpose, nmpy.array, nmpy.array)
    titles = ("COL x DEP", "ROW x DEP", "ROW x COL")
    for d_idx, (operation, title) in enumerate(zip(ops, titles)):
        figure = pypl.figure()
        axes = figure.add_subplot()
        axes.set_title(title)
        _ = axes.matshow(operation(nmpy.amax(volume_, axis=d_idx)))

# --- Execution preparation
all_titles = []
all_enhanced = []
all_scale_maps = []
# from guppy import hpy as hpy_t
# mem_tracker = hpy_t()

# --- Frangi3
for method in ("python", "c"):
    print(f"Frangi3[{method.capitalize()}]")
    start_time = time.time()
    # mem_tracker.setrelheap()
    enhanced, scale_map = FrangiEnhancement(
        volume_,
        SCALE_RANGE,
        scale_step=SCALE_STEP,
        alpha=ALPHA,
        beta=BETA,
        frangi_c=FRANGI_C,
        method=method,
    )
    print(f"Elapsed time: {time.time() - start_time}\n")
    # print(str(mem_tracker.heap()).splitlines()[0])
    all_titles.append(f"Frangi3[{method.capitalize()}]")
    all_enhanced.append(enhanced)
    all_scale_maps.append(scale_map)

# --- Standard methods
scales = ScalesFromRangeAndStep(SCALE_RANGE, SCALE_STEP)
for method in METHODS:
    print(method.upper())

    start_time = time.time()
    # mem_tracker.setrelheap()

    if method == "ellisdg":
        enhanced, scale_map = FRANGI.frangi(
            volume_,
            scale_range=SCALE_RANGE,
            scale_step=SCALE_STEP,
            alpha=ALPHA,
            beta=BETA,
            frangi_c=FRANGI_C,
            black_vessels=False,
        )
    elif method == "hessian":
        enhanced = fltr.hessian(
            volume_,
            sigmas=scales,
            alpha=ALPHA,
            beta=BETA,
            gamma=FRANGI_C,
            black_ridges=False,
        )
        scale_map = None
    elif method == "meijering":
        enhanced = fltr.meijering(
            volume_, sigmas=scales, alpha=ALPHA, black_ridges=False
        )
        scale_map = None
    elif method == "sato":
        enhanced = fltr.sato(volume_, sigmas=scales, black_ridges=False)
        scale_map = None
    elif method == "skimage":
        enhanced = fltr.frangi(
            volume_,
            sigmas=scales,
            alpha=ALPHA,
            beta=BETA,
            gamma=FRANGI_C,
            black_ridges=False,
        )
        scale_map = None
    else:
        print(f"{method}: Unknown method; Skipping")
        continue

    print(f"Elapsed time: {time.time() - start_time}\n")
    # print(str(mem_tracker.heap()).splitlines()[0])

    all_titles.append(method.capitalize())
    all_enhanced.append(enhanced)
    all_scale_maps.append(scale_map)

# --- Results display
for title, enhanced, scale_map in zip(all_titles, all_enhanced, all_scale_maps):
    if scale_map is None:
        base_subplot = 120
    else:
        base_subplot = 140

    figure = pypl.figure()
    figure.set_constrained_layout(True)
    figure.set_constrained_layout_pads(wspace=0, hspace=0)
    axes = figure.add_subplot(base_subplot + 1)
    axes.set_title(title)
    _ = axes.matshow(enhanced[:, :, max(RADII) + margin])
    # figure.colorbar(plot, ax=axes)

    skeleton = SkeletonOfResponse(enhanced)
    # print(f"Skeleton points: {nmpy.count_nonzero(skeleton)}")

    axes = figure.add_subplot(base_subplot + 2)
    axes.set_title(title)
    _ = axes.matshow(nmpy.amax(enhanced * skeleton, axis=-1))
    # figure.colorbar(plot, ax=axes)

    if scale_map is not None:
        axes = figure.add_subplot(base_subplot + 3)
        axes.set_title(title + " scales")
        plot = axes.matshow(scale_map[:, :, max(RADII) + margin])
        # figure.colorbar(plot, ax=axes)

        axes = figure.add_subplot(base_subplot + 4)
        axes.set_title(title + " scales")
        _ = axes.matshow(nmpy.amax(scale_map * skeleton, axis=-1))
        # figure.colorbar(plot, ax=axes)

pypl.show()

# print(f"I.{intensity}-R.{radius}...")
# per_bar_volume = nmpy.zeros(shape, dtype=nmpy.float32)
# per_bar_volume[
#     row : (row + bar_n_rows), col + margin, max(RADII) + margin
# ] = 1.0
#
# per_bar_volume = spim.uniform_filter(
#     per_bar_volume, size=(1, 2 * radius + 1, 2 * radius + 1)
# )
# volume_[per_bar_volume > 0.0] = intensity
#
# per_bar_volume = spim.gaussian_filter(per_bar_volume, radius)
# slice = per_bar_volume[row + bar_n_rows // 2, ...]
# threshold = nmpy.max(per_bar_volume)
# t_step = 0.02 * threshold
# area = 0.0
# target_area = nmpy.pi * radius ** 2
# while area < target_area:
#     threshold -= t_step
#     area = nmpy.count_nonzero(slice >= threshold)
# volume_[per_bar_volume > threshold] = intensity

# import skimage.measure as ms_
# from mpl_toolkits.mplot3d import Axes3D

# axes = figure.add_subplot(111, projection=Axes3D.name)
# verts, faces = ms_.marching_cubes(
#     volume_, level=0.0, spacing=(0.5, 0.5, 0.5), method='_lorensen'
# )
# _ = axes.plot_trisurf(verts[:, 0], verts[:, 1], faces, verts[:, 2], cmap="Spectral", lw=1)
