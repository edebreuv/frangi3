# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2019)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

from __future__ import annotations

import dataclasses as dtcl
import sys as sstm
from argparse import ArgumentParser as argument_parser_t
from os import path
from typing import Callable

import main as frng
import matplotlib.pyplot as pypl
import matplotlib.widgets as wdgt
import mpl_toolkits.axes_grid1 as grid
import numpy as nmpy
import skimage.filters as fltr
import skimage.io as skio
from matplotlib.backend_bases import Event as event_t
from matplotlib.backend_bases import KeyEvent as key_event_t
from matplotlib.gridspec import GridSpec as grid_spec_t
from matplotlib.image import AxesImage as axes_image_t


array_t = nmpy.ndarray
axes_t = pypl.Axes
figure_t = pypl.Figure
on_span_select_t = Callable[[float, float], None]


@dtcl.dataclass(slots=True, repr=False, eq=False)
class frangi3_figure_t:
    image: array_t = None
    mip_axis: int = None
    bright_on_dark: bool = None
    root: figure_t = None
    grid_spec: grid_spec_t = None
    cross: wdgt.MultiCursor = None
    thresholds: thresholds_t = None
    scale_slider: wdgt.Slider = None
    parameters_sliders: list[wdgt.Slider] = None
    enhanced: array_t = None
    plate_map: array_t = None
    blob_map: array_t = None
    bckgnd: array_t = None
    eig_condition: array_t = None
    axes: dict[str, extended_axes_t] = dtcl.field(default_factory=dict)

    def __post_init__(self) -> None:
        """"""
        pypl.rcParams.update({"figure.autolayout": True})

        figure = pypl.figure()
        grid_spec = figure.add_gridspec(
            nrows=6,
            ncols=3,
            left=0.125,
            bottom=0.025,
            right=0.9,
            top=0.975,
            wspace=0.1,
            hspace=0.7,
            height_ratios=[0.025, 0.45, 0.45, 0.025, 0.025, 0.025],
        )
        self.root = figure
        self.grid_spec = grid_spec

        uids = ("input", "frangi", "detection", "plate", "blob", "bckgnd")
        titles = ("Input", "Frangi", "Detection", "Plate Map", "Blob Map", "Background")
        wo_tls = (False, True, True, True, True, True)
        for position, (uid, title, wo_tl) in enumerate(zip(uids, titles, wo_tls)):
            row, col = (position // 3) + 1, position % 3
            self.axes[uid] = extended_axes_t(
                self._NewAxes(title, row, col, wo_tick_labels=wo_tl)
            )

        self._AddHysteresisThresholds()

        # Funny fact: if deactivated right away, previous crosses remain visible as cursor moves!
        self.cross = wdgt.MultiCursor(
            self.root.canvas,
            [elm.root for elm in self.axes.values()],
            color="g",
            lw=1,
            horizOn=True,
        )
        self.root.canvas.mpl_connect("key_press_event", self._OnKeyPress)

        scale_slider = self._NewSlider(
            0, "Scale", 0.0, 5.0, 1.0, self._UpdateComponentsForNewScale
        )
        self.scale_slider = scale_slider

        parameters_sliders = []
        alpha_details = ("Alpha", 0.0, 5.0, 0.5)
        beta_details = ("Beta", 0.0, 5.0, 0.5)
        frangi_c_details = ("Frangi_c", 0.0, 1000, 500.0)
        all_details = (alpha_details, beta_details, frangi_c_details)
        for position, prm_details in enumerate(all_details, start=3):
            slider = self._NewSlider(
                position, *prm_details, self._UpdateEnhancementForNewParameter
            )
            parameters_sliders.append(slider)
        self.parameters_sliders = parameters_sliders

    def SetImage(self, image: array_t, mip_axis: int, bright_on_dark: bool) -> None:
        """"""
        self.image = image.copy()
        self.mip_axis = mip_axis
        self.bright_on_dark = bright_on_dark

        self.axes["input"].UpdateWithImage(
            MIP(self.image, self.mip_axis), with_colorbar=True, cmap="gray"
        )

        self._UpdateComponentsForNewScale(None)
        self.thresholds.UpdateDisplay()

    def _NewAxes(
        self, label: str, row: int, col: int, wo_tick_labels: bool = False
    ) -> axes_t:
        """"""
        output = self.root.add_subplot(self.grid_spec[row, col], label=label)
        output.xaxis.tick_top()
        if wo_tick_labels:
            output.set_xticklabels(())
            output.set_yticklabels(())
        output.set_ylabel(label)
        output.format_coord = lambda x, y: f"R:{int(y + 0.5)},C:{int(x + 0.5)}"

        return output

    def _NewSlider(
        self,
        position: int,
        label: str,
        value_min: float,
        value_max: float,
        value_init: float,
        UpdateFrangi: Callable,
    ) -> wdgt.Slider:
        """"""
        slider_room = self.root.add_subplot(self.grid_spec[position, :], label=label)
        output = wdgt.Slider(
            slider_room,
            label,
            value_min,
            value_max,
            valinit=value_init,
            closedmin=False,
        )
        output.on_changed(UpdateFrangi)

        return output

    def _AddHysteresisThresholds(self) -> None:
        """"""
        self.thresholds = thresholds_t()
        self.thresholds.AttachToAxes(
            self.axes["detection"].root, self._UpdateDetectionForNewFactors
        )

    def _UpdateComponentsForNewScale(self, _: event_t | None) -> None:
        """"""
        scale = self.scale_slider.val

        plate_map, blob_map, bckgnd, eig_condition = frng.FrangiSingleScaleComponents(
            self.image,
            scale,
            frng.FillHessianMatricesW2FirstOrderFDs,
            bright_on_dark=self.bright_on_dark,
        )
        self.plate_map = plate_map
        self.blob_map = blob_map
        self.bckgnd = bckgnd
        self.eig_condition = eig_condition

        all_maps = (self.plate_map, self.blob_map, self.bckgnd)
        all_axes = (self.axes["plate"], self.axes["blob"], self.axes["bckgnd"])
        all_w_cbar = (False, False, True)
        for image, axes, w_cbar in zip(all_maps, all_axes, all_w_cbar):
            axes.UpdateWithImage(
                MIP(image, self.mip_axis, has_nan=True),
                with_colorbar=w_cbar,
                cmap="hot",
            )

        self._UpdateEnhancementForNewParameter(None)

    def _UpdateEnhancementForNewParameter(self, _: event_t | None) -> None:
        """"""
        alpha = self.parameters_sliders[0].val
        beta = self.parameters_sliders[1].val
        frangi_c = self.parameters_sliders[2].val

        self.enhanced = frng.FrangiSingleScaleCombination(
            self.plate_map,
            self.blob_map,
            self.bckgnd,
            self.eig_condition,
            alpha=alpha,
            beta=beta,
            frangi_c=frangi_c,
        )

        self.axes["frangi"].UpdateWithImage(
            MIP(self.enhanced, self.mip_axis), with_colorbar=True, cmap="hot"
        )

        factors = self.thresholds.MinMax()
        self._UpdateDetectionForNewFactors(*factors)

    def _UpdateDetectionForNewFactors(
        self, min_factor: float, max_factor: float
    ) -> None:
        """"""
        max_value = nmpy.max(self.enhanced)
        detection = fltr.apply_hysteresis_threshold(
            self.enhanced, min_factor * max_value, max_factor * max_value
        )
        self.axes["detection"].UpdateWithImage(
            MIP(detection, self.mip_axis), cmap="gray"
        )

    def _OnKeyPress(self, event: key_event_t) -> None:
        """"""
        if event.key == "c":
            self.cross.set_active(not self.cross.get_active())

    def PrintMinsAndMaxes(self) -> None:
        """"""
        print(
            f"Plate map:      [{nmpy.nanmin(self.plate_map)},{nmpy.nanmax(self.plate_map)}]\n"
            f"Blob map:       [{nmpy.nanmin(self.blob_map)},{nmpy.nanmax(self.blob_map)}]\n"
            f"Background map: [{nmpy.min(self.bckgnd)},{nmpy.max(self.bckgnd)}]"
        )


@dtcl.dataclass(slots=True, repr=False, eq=False)
class extended_axes_t:
    root: axes_t
    image: axes_image_t | None = None
    colorbar: axes_t | None = None

    def UpdateWithImage(
        self, image: array_t, with_colorbar: bool = False, **kwargs
    ) -> None:
        """"""
        self._RemoveImage()
        self.image = self.root.matshow(image, **kwargs)
        if with_colorbar:
            self._RemoveColorbar()
            self._AddColorbar()

    def _RemoveImage(self) -> None:
        """"""
        if self.image is not None:
            self.image.remove()
            self.image = None

    def _AddColorbar(self) -> None:
        """"""
        divider = grid.make_axes_locatable(self.root)
        self.colorbar = divider.append_axes("bottom", size="6%", pad="2%")
        self.root.figure.colorbar(
            self.image, cax=self.colorbar, orientation="horizontal"
        )

    def _RemoveColorbar(self) -> None:
        """"""
        if self.colorbar is not None:
            self.colorbar.remove()
            self.colorbar = None


@dtcl.dataclass(slots=True, repr=False, eq=False)
class thresholds_t:
    root: wdgt.SpanSelector = None
    container: axes_t = None
    target_axes: axes_t = None
    ExternalOnSelect: on_span_select_t = None
    display: pypl.Text = None

    def AttachToAxes(self, axes: axes_t, OnSelect: on_span_select_t) -> None:
        """"""
        self.target_axes = axes
        self.ExternalOnSelect = OnSelect

        divider = grid.make_axes_locatable(self.target_axes)
        container = divider.append_axes("bottom", size="6%", pad="2%")
        container.set_xticks(nmpy.linspace(0.0, 1.0, num=11))
        container.set_yticks(())
        container.set_xticklabels(())
        container.tick_params(axis="x", direction="in", bottom=True, top=True)
        container.format_coord = lambda x, y: f"T:{x:.2f}"
        self.container = container

        style = {"facecolor": "green", "alpha": 0.5}
        self.root = wdgt.SpanSelector(
            self.container,
            self._OnSelect,
            "horizontal",
            rectprops=style,
            span_stays=True,
        )

        self.display = self.target_axes.text(10, 0, "", fontsize="small", color="green")

    def MinMax(self) -> tuple[float, float]:
        """"""
        span = self.root.stay_rect

        if span.get_width() > 0.0:
            min_factor = span.get_x()
            max_factor = span.get_x() + span.get_width()
        else:
            min_factor = 0.3
            max_factor = 0.6

        return min_factor, max_factor

    def UpdateDisplay(self) -> None:
        """"""
        self._OnSelect(*self.MinMax())

    def _OnSelect(self, min_factor: float, max_factor: float) -> None:
        """"""
        self.ExternalOnSelect(min_factor, max_factor)

        display = self.display

        x_pos = display.get_position()[0]
        y_pos = self.target_axes.get_ylim()[0]
        display.set_position((x_pos, y_pos))
        display.set_text(f"m: {min_factor:.2f}\nM: {max_factor:.2f}")


def MIP(image: array_t, axis: int, has_nan: bool = False) -> array_t:
    """"""
    if has_nan:
        return nmpy.nanmax(image, axis=axis)
    else:
        return nmpy.max(image, axis=axis)


def SliceFromString(slice_a_str: str, max_value: int) -> slice | None:
    """"""
    if slice_a_str == ":":
        return slice(None)

    slice_as_str_lst = slice_a_str.split(":")
    if slice_as_str_lst.__len__() != 2:
        return None

    slice_as_int_lst = []
    for elm in slice_as_str_lst:
        if elm.__len__() > 0:
            try:
                value = int(elm)
            except ValueError:
                return None
            #
            slice_as_int_lst.append(value)
        else:
            slice_as_int_lst.append(None)

    if slice_as_int_lst[0] is None:
        slice_as_int_lst[0] = 0
    if slice_as_int_lst[1] is None:
        slice_as_int_lst[1] = max_value
    if (
        (slice_as_int_lst[0] < 0)
        or (slice_as_int_lst[1] > max_value)
        or (slice_as_int_lst[1] < slice_as_int_lst[0] + 3)
    ):
        return None

    return slice(*slice_as_int_lst)


_RANGE_HELP = (
    "Range of processing in the first ({0}) dimension. "
    "Default: use the whole range. "
    "Valid entry=Numpy slice: [value_1]:[value2], "
    "where value_1>=0, value_2<=number_of_{0}s, value_2>=value_1 + 3."
)
_RANGE_HELP_SEE = 'See "--{0}", replacing "{1}" in the text with "{2}".'


def Main() -> None:
    """"""
    parser = argument_parser_t(
        description="Interactive Frangi Enhancement of a 3-D image.", allow_abbrev=False
    )
    parser.add_argument(
        "image",
        help='A 3-dimensional image loadable by the function "imread" of Scikit-Image '
        "(https://scikit-image.org/docs/stable/api/skimage.io.html#skimage.io.imread).",
    )
    parser.add_argument(
        "--mip",
        type=int,
        choices=range(-1, 3),
        help="The axis along which to perform Maximum Intensity Projection (MIP). "
        "Default: the axis of minimal length of the image. "
        "Value -1 denotes the last axis.",
    )
    parser.add_argument(
        "--bright",
        choices=["t", "f", "true", "false"],
        default="t",
        help="Does the image contain bright objects over a dark background "
        "(or the opposite)? "
        "Default: true.",
    )
    parser.add_argument("--row", default=":", help=_RANGE_HELP.format("row"))
    parser.add_argument(
        "--col", default=":", help=_RANGE_HELP_SEE.format("row", "row", "column")
    )
    parser.add_argument(
        "--dep", default=":", help=_RANGE_HELP_SEE.format("row", "row", "depth")
    )

    arguments = parser.parse_args()

    image = None
    if path.isfile(arguments.image):
        image = skio.imread(arguments.image)
    else:
        print(f"{arguments.image}: Not a readable file.")
        sstm.exit(1)
    mip_axis = arguments.mip
    if mip_axis is None:
        mip_axis = nmpy.argmin(image.shape).item()
    bright_on_dark = arguments.bright.startswith("t")

    slices = []
    slice_names = ("row", "col", "dep")
    lengths = image.shape
    for d_idx, name in enumerate(slice_names):
        one_slice = SliceFromString(getattr(arguments, name), lengths[d_idx])
        if one_slice is None:
            parser.print_help()
            sstm.exit(2)
        slices.append(one_slice)
    image = image[slices[0], slices[1], slices[2]]

    figure = frangi3_figure_t()
    figure.SetImage(image, mip_axis, bright_on_dark)
    pypl.show()


if __name__ == "__main__":
    #
    Main()
