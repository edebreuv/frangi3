# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2019)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""
Python version inspired from: https://github.com/ellisdg/frangi3d
"""
import ctypes as ctps
import multiprocessing as mlpr
import warnings as wrng
from multiprocessing.sharedctypes import Array as shared_array_t
from os import name as OS_NAME
from os import path
from typing import Callable, Optional, Sequence

import numpy as nmpy
import scipy.ndimage as spim
import skimage.filters as fltr

try:
    import itk as itkl
except ModuleNotFoundError:
    itkl = None


_folder, _ = path.split(path.realpath(__file__))
if _folder.__len__() == 0:
    _folder = "."
_extension_path = path.join(_folder, OS_NAME + ".so")
try:
    _three_ints = 3 * (ctps.c_int,)
    _two_pointers = 2 * (ctps.c_void_p,)
    _six_floats = 6 * (ctps.c_float,)
    _c_extension = ctps.CDLL(_extension_path)
    _ComputeEnhancementInC = _c_extension.ComputeFrangiResponseFromRaw
    _ComputeEnhancementInC.argtypes = (
        ctps.c_void_p,
        *_three_ints,
        ctps.c_bool,
        *_two_pointers,
        *_six_floats,
    )
    _ComputeEnhancementInC.restype = None
except Exception as exc:
    wrng.warn(
        f"{_extension_path}: Error when loading extension (see exception below)\n"
        f"{exc}\n"
        f"=> Falling back to Python implementation",
        RuntimeWarning,
    )
    _ComputeEnhancementInC = None


array_t = nmpy.ndarray
h_matrices_fct_t = Callable[[array_t, array_t], None]
enhancement_one_scale_fct_t = Callable[
    [
        array_t,
        float,
        h_matrices_fct_t,
        float,
        float,
        float,
        bool,
        Optional[shared_array_t],
    ],
    Optional[array_t],
]


_across_scales_shared_hessian_matrices = None


def FrangiEnhancement(
    volume: array_t,
    scale_range: tuple[float, float],
    /,
    *,
    scale_step: float = 2.0,
    alpha: float = 0.5,
    beta: float = 0.5,
    frangi_c: float = 500.0,
    bright_on_dark: bool = True,
    in_parallel: bool = False,
    method: str = "python",  # python, skimage, itk, c
    differentiation_mode: str = "FD11",  # FD11, FD2, convolution
) -> tuple[array_t, array_t]:
    """"""
    global _across_scales_shared_hessian_matrices

    if (method == "c") and (_ComputeEnhancementInC is None):
        method = "python"
        in_parallel = True
        differentiation_mode = "FD11"

    if method == "c":
        volume = volume.astype(nmpy.float32, order="F", copy=False)
        if not volume.flags.f_contiguous:
            raise RuntimeError(
                "Volume array not Fortran-contiguous; Please contact developer."
            )

        enhanced = nmpy.empty_like(volume)
        scale_map = nmpy.empty_like(volume)
        height, width, depth = volume.shape

        _ComputeEnhancementInC(
            volume.ctypes.data,
            width,
            height,
            depth,
            bright_on_dark,
            enhanced.ctypes.data,
            scale_map.ctypes.data,
            scale_range[0],
            scale_range[1],
            scale_step,
            alpha,
            beta,
            frangi_c,
        )
    #
    else:
        volume = volume.astype(nmpy.float32, copy=False)
        scales = ScalesFromRangeAndStep(scale_range, scale_step)

        if in_parallel and (mlpr.get_start_method(allow_none=False) == "fork"):
            # Do not share Hessian matrices storage across scales
            _across_scales_shared_hessian_matrices = None
            EnhancementAllScales = _EnhancementWParallelScales
        else:
            # Do share Hessian matrices storage across scales
            _across_scales_shared_hessian_matrices = nmpy.empty(
                (*volume.shape, 3, 3), dtype=nmpy.float32
            )
            EnhancementAllScales = _EnhancementWSequentialScales

        HessianMatrices_fct = None
        if method == "python":
            if differentiation_mode == "FD11":
                HessianMatrices_fct = FillHessianMatricesW2FirstOrderFDs
            elif differentiation_mode == "FD2":
                HessianMatrices_fct = FillHessianMatricesW1SecondOrderFD
            elif differentiation_mode == "convolution":
                HessianMatrices_fct = FillHessianMatricesByConvolution
            else:
                raise ValueError(
                    f"{differentiation_mode}: Invalid differentiation mode"
                )
            EnhancementOneScale = _EnhancementOneScaleWPython
        elif method == "skimage":
            EnhancementOneScale = _EnhancementOneScaleWSKImage
        elif method == "itk":
            EnhancementOneScale = _EnhancementOneScaleWITK
        else:
            raise ValueError(f"{method}: Invalid computation method")

        enhanced, scale_map = EnhancementAllScales(
            volume,
            scales,
            HessianMatrices_fct,
            alpha=alpha,
            beta=beta,
            frangi_c=frangi_c,
            bright_on_dark=bright_on_dark,
            EnhancementOneScale=EnhancementOneScale,
        )

        _across_scales_shared_hessian_matrices = None

    return enhanced, scale_map


def ScalesFromRangeAndStep(
    scale_range: tuple[float, float], scale_step: float, /
) -> tuple[float, ...]:
    """"""
    min_scale, max_scale = scale_range
    if min_scale > max_scale:
        return ()

    scales = [min_scale]
    while True:
        next_scale = scales[-1] + scale_step
        if next_scale <= max_scale:
            scales.append(next_scale)
        else:
            break

    return tuple(scales)


def FrangiSingleScaleEnhancement(
    volume: array_t,
    scale: float,
    FillHessianMatrices: h_matrices_fct_t,
    /,
    *,
    alpha: float = 0.5,
    beta: float = 0.5,
    frangi_c: float = 500.0,
    bright_on_dark: bool = True,
) -> array_t:
    """"""
    plate_map, sq_blob_map, sq_bckgnd, eig_condition = FrangiSingleScaleComponents(
        volume,
        scale,
        FillHessianMatrices,
        bright_on_dark=bright_on_dark,
    )

    return FrangiSingleScaleCombination(
        plate_map,
        sq_blob_map,
        sq_bckgnd,
        eig_condition,
        alpha=alpha,
        beta=beta,
        frangi_c=frangi_c,
    )


def FrangiSingleScaleComponents(
    volume: array_t,
    scale: float,
    FillHessianMatrices: h_matrices_fct_t,
    /,
    *,
    bright_on_dark: bool = True,
) -> tuple[array_t, array_t, array_t, array_t]:
    """"""
    sorted_evs = _EigenValuesOfHessianMatrices(volume, scale, FillHessianMatrices)
    # No need to take the absolute value of the eigenvalues because "everything" will be squared in the component
    # combination. However, an absolute value must be inserted before square root below.

    # Absolute plate, blob, and background maps
    plate_map = _DivisionWithNaN(sorted_evs[..., 1], sorted_evs[..., 2])
    sq_blob_map = _DivisionWithNaN(
        sorted_evs[..., 0] ** 2, nmpy.fabs(nmpy.prod(sorted_evs[..., 1:], axis=-1))
    )
    sq_bckgnd = nmpy.sum(sorted_evs**2, axis=-1)

    if bright_on_dark:
        eig_condition = nmpy.any(sorted_evs[..., 1:] > 0.0, axis=-1)
    else:
        eig_condition = nmpy.any(sorted_evs[..., 1:] < 0.0, axis=-1)

    return plate_map, sq_blob_map, sq_bckgnd, eig_condition


def FrangiSingleScaleCombination(
    plate_map: array_t,
    sq_blob_map: array_t,
    sq_bckgnd: array_t,
    eig_condition: array_t,
    /,
    *,
    alpha: float = 0.5,
    beta: float = 0.5,
    frangi_c: float = 500.0,
) -> array_t:
    """"""
    # Normalized plate, blob, and background maps
    plate_factor = -0.5 / alpha**2
    blob_factor = -0.5 / beta**2
    plate_map = 1.0 - nmpy.exp(plate_factor * plate_map**2)
    blob_map = nmpy.exp(blob_factor * sq_blob_map)

    enhanced = plate_map * blob_map

    if frangi_c > 0.0:
        bckgnd_factor = -0.5 / frangi_c**2
        bckgnd = 1.0 - nmpy.exp(bckgnd_factor * sq_bckgnd)
        enhanced *= bckgnd

    invalid_condition = nmpy.isfinite(enhanced)
    # Split computation to (try to) avoid multiple allocations
    nmpy.logical_not(invalid_condition, out=invalid_condition)
    nmpy.logical_or(eig_condition, invalid_condition, out=invalid_condition)
    enhanced[invalid_condition] = 0.0

    return enhanced


def _EigenValuesOfHessianMatrices(
    volume: array_t, scale: float, FillHessianMatrices: h_matrices_fct_t, /
) -> array_t:
    """"""
    global _across_scales_shared_hessian_matrices

    if _across_scales_shared_hessian_matrices is None:
        # Private Hessian matrices storage (parallel version)
        hessian_matrices = nmpy.empty((*volume.shape, 3, 3), dtype=nmpy.float32)
    else:
        # Shared Hessian matrices storage (sequential version)
        hessian_matrices = _across_scales_shared_hessian_matrices

    if (scale == 0.0) and (FillHessianMatrices is FillHessianMatricesByConvolution):
        FillHessianMatrices = FillHessianMatricesW2FirstOrderFDs
    if FillHessianMatrices is FillHessianMatricesByConvolution:
        FillHessianMatricesByConvolution(hessian_matrices, volume, scale=scale)
        # Normalization: scale^derivative_order
        hessian_matrices *= scale**2
    else:
        if scale > 0.0:
            # Normalization: scale^derivative_order
            volume = scale**2 * spim.gaussian_filter(volume, scale)

        FillHessianMatrices(hessian_matrices, volume)

    # Only the lower triangular part of "hessian_matrices" must be set
    eigenvalues = nmpy.linalg.eigvalsh(hessian_matrices)

    # Sorted by abs value
    indexing = nmpy.ix_(*tuple(nmpy.arange(length) for length in eigenvalues.shape))
    indexing_last = nmpy.fabs(eigenvalues).argsort(axis=-1)
    sorted_eigenvalues = eigenvalues[indexing[:-1] + (indexing_last,)]
    # ReorderByAbsoluteValue = lambda ary: ary[nmpy.fabs(ary).argsort()]
    # sorted_eigenvalues = nmpy.apply_along_axis(ReorderByAbsoluteValue, -1, eigenvalues)

    return sorted_eigenvalues


def FillHessianMatricesW2FirstOrderFDs(
    hessian_matrices: array_t, volume: array_t, /
) -> None:
    """
    Only fill the lower triangular part
    """
    Dx, Dy, Dz = nmpy.gradient(volume)

    (
        hessian_matrices[..., 0, 0],
        hessian_matrices[..., 1, 0],
        hessian_matrices[..., 2, 0],
    ) = nmpy.gradient(Dx)

    hessian_matrices[..., 1, 1], hessian_matrices[..., 2, 1] = nmpy.gradient(
        Dy, axis=(1, 2)
    )

    hessian_matrices[..., 2, 2] = nmpy.gradient(Dz, axis=2)


def FillHessianMatricesW1SecondOrderFD(
    hessian_matrices: array_t, volume: array_t, /
) -> None:
    """
    Only fill the lower triangular part
    """
    for axis in range(3):
        hessian_matrices[..., axis, axis] = (
            nmpy.roll(volume, 1, axis=axis)
            - 2.0 * volume
            + nmpy.roll(volume, -1, axis=axis)
        )

    for axes in ((0, 1), (0, 2), (1, 2)):
        hessian_matrices[..., axes[1], axes[0]] = 0.25 * (
            nmpy.roll(volume, 1, axis=axes)
            + nmpy.roll(volume, -1, axis=axes)
            - nmpy.roll(nmpy.roll(volume, 1, axis=axes[0]), -1, axis=axes[1])
            - nmpy.roll(nmpy.roll(volume, -1, axis=axes[0]), 1, axis=axes[1])
        )


def FillHessianMatricesByConvolution(
    hessian_matrices: array_t, volume: array_t, /, *, scale: float = 1.0
) -> None:
    """
    Only fill the lower triangular part
    """
    all_axes = ((0, 0), (1, 0), (2, 0), (1, 1), (2, 1), (2, 2))
    orders = ((2, 0, 0), (1, 1, 0), (1, 0, 1), (0, 2, 0), (0, 1, 1), (0, 0, 2))
    for axes, order in zip(all_axes, orders):
        spim.gaussian_filter(
            volume, scale, order=order, output=hessian_matrices[..., axes[0], axes[1]]
        )


def _EnhancementOneScaleWPython(
    volume: array_t,
    scale: float,
    FillHessianMatrices: h_matrices_fct_t,
    /,
    *,
    alpha: float = 0.5,
    beta: float = 0.5,
    frangi_c: float = 500.0,
    bright_on_dark: bool = True,
    shared_enhanced: shared_array_t = None,
) -> array_t | None:
    """"""
    print(f"    scale[Python]={scale}")

    enhanced = FrangiSingleScaleEnhancement(
        volume,
        scale,
        FillHessianMatrices,
        alpha=alpha,
        beta=beta,
        frangi_c=frangi_c,
        bright_on_dark=bright_on_dark,
    )

    if shared_enhanced is None:
        return enhanced
    else:
        shared_enhanced[:] = enhanced.flatten()[:]


def _EnhancementOneScaleWSKImage(
    volume: array_t,
    scale: float,
    _: h_matrices_fct_t,
    /,
    *,
    alpha: float = 0.5,
    beta: float = 0.5,
    frangi_c: float = 500.0,
    bright_on_dark: bool = True,
    shared_enhanced: shared_array_t = None,
) -> array_t | None:
    """"""
    print(f"    scale[SKImage]={scale}")

    enhanced = fltr.frangi(
        volume,
        sigmas=(scale,),
        alpha=alpha,
        beta=beta,
        gamma=frangi_c,
        black_ridges=not bright_on_dark,
    )

    if shared_enhanced is None:
        return enhanced
    else:
        shared_enhanced[:] = enhanced.flatten()[:]


def _EnhancementOneScaleWITK(
    volume: array_t,
    scale: float,
    _: h_matrices_fct_t,
    /,
    *,
    alpha: float = 0.5,
    beta: float = 2.0,
    __: float = 500.0,  # frangi_c
    bright_on_dark: bool = True,
    shared_enhanced: shared_array_t = None,
) -> array_t | None:
    """"""
    if itkl is None:
        raise RuntimeError('Module "itk" not installed')

    print(f"    scale[ITK]={scale}")

    if alpha >= beta:
        raise ValueError(
            f"Parameter alpha (passed {alpha}) must be strictly smaller than beta (passed {beta})"
        )
    if not bright_on_dark:
        # ITK version applies only to bright objects on dark background
        volume = (nmpy.max(volume) - volume) + nmpy.min(volume)

    volume = itkl.image_view_from_array(volume)
    hessian_image = itkl.hessian_recursive_gaussian_image_filter(volume, sigma=scale)
    # times_filter = itkl.MultiplyImageFilter.New()
    # times_filter.SetInput(hessian_image)
    # times_filter.SetConstant(scale ** 2)
    # hessian_image = times_filter.GetOutput()

    vesselness_filter = itkl.Hessian3DToVesselnessMeasureImageFilter[
        itkl.ctype("float")
    ].New()
    vesselness_filter.SetInput(hessian_image)
    vesselness_filter.SetAlpha1(alpha)
    vesselness_filter.SetAlpha2(beta)

    # Do not use view-version here since vesselness_filter is local
    enhanced = itkl.array_from_image(vesselness_filter)

    if shared_enhanced is None:
        return enhanced
    else:
        shared_enhanced[:] = enhanced.flatten()[:]


def _EnhancementWSequentialScales(
    volume: array_t,
    scales: Sequence[float],
    FillHessianMatrices: h_matrices_fct_t,
    /,
    *,
    alpha: float = 0.5,
    beta: float = 0.5,
    frangi_c: float = 500.0,
    bright_on_dark: bool = True,
    EnhancementOneScale: enhancement_one_scale_fct_t = None,
) -> tuple[array_t, array_t]:
    """"""
    enhanced = None
    scale_map = None

    for s_idx, scale in enumerate(scales):
        # noinspection PyArgumentList
        single_enhanced = EnhancementOneScale(
            volume,
            scale,
            FillHessianMatrices,
            alpha=alpha,
            beta=beta,
            frangi_c=frangi_c,
            bright_on_dark=bright_on_dark,
        )

        if s_idx > 0:
            larger_map = single_enhanced > enhanced
            enhanced[larger_map] = single_enhanced[larger_map]
            scale_map[larger_map] = scale
        else:
            enhanced = single_enhanced
            scale_map = nmpy.full(volume.shape, scale, dtype=nmpy.float32)

    return enhanced, scale_map


def _EnhancementWParallelScales(
    volume: array_t,
    scales: Sequence[float],
    FillHessianMatrices: h_matrices_fct_t,
    /,
    *,
    alpha: float = 0.5,
    beta: float = 0.5,
    frangi_c: float = 500.0,
    bright_on_dark: bool = True,
    EnhancementOneScale: enhancement_one_scale_fct_t = None,
) -> tuple[array_t, array_t]:
    """"""
    fixed_args = {
        "alpha": alpha,
        "beta": beta,
        "frangi_c": frangi_c,
        "bright_on_dark": bright_on_dark,
    }

    shared_enhanced_s = tuple(
        shared_array_t(ctps.c_float, volume.size) for ___ in scales
    )
    processes = tuple(
        mlpr.Process(
            target=EnhancementOneScale,
            args=(volume, scale, FillHessianMatrices),
            kwargs={
                **fixed_args,
                "shared_enhanced": shared_enhanced,
            },
        )
        for scale, shared_enhanced in zip(scales, shared_enhanced_s)
    )

    for process in processes:
        process.start()
    for process in processes:
        process.join()

    enhanced = nmpy.array(shared_enhanced_s[0])
    scale_map = nmpy.full(volume.size, scales[0], dtype=nmpy.float32)
    for s_idx, shared_enhanced in enumerate(shared_enhanced_s[1:], start=1):
        single_enhanced = nmpy.array(shared_enhanced)
        larger_map = single_enhanced > enhanced
        enhanced[larger_map] = single_enhanced[larger_map]
        scale_map[larger_map] = scales[s_idx]

    enhanced = enhanced.astype(nmpy.float32, copy=False).reshape(volume.shape)
    scale_map = scale_map.astype(nmpy.float32, copy=False).reshape(volume.shape)

    return enhanced, scale_map


def _DivisionWithNaN(array_1: array_t, array_2: array_t, /) -> array_t:
    """"""
    null_map = array_2 == 0.0
    if null_map.any():
        denominator = array_2.copy()
        denominator[null_map] = nmpy.nan
    else:
        denominator = array_2

    return array_1 / denominator


# Scale-space with uniform kernel:
#     spim.uniform_filter(volume, size=2 * (int(nmpy.around(2.0*scale)) // 2) + 1)
